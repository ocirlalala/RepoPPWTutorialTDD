var print,erase;
    // -------------------------- Calculator ------------------------------ //
function go(x) {
	if (x === 'ac') {
		print.value = "";
	}
	else if (x === 'eval') {
		if (print.value.includes('sin') || print.value.includes('tan') || print.value.includes('log')) {
			var indexOpen = print.value.indexOf('(');
			var indexClose = print.value.indexOf(')');

			if (print.value.includes('sin')) {
				print.value = Math.sin(print.value.substring(indexOpen + 1, indexClose));
			}
			else if (print.value.includes('tan')) {
				print.value = Math.tan(print.value.substring(indexOpen + 1, indexClose));
			}
			else if (print.value.includes('log')) {
				print.value = Math.log(print.value.substring(indexOpen + 1, indexOpen));
			}
			erase = true;
		}
		else {
			print.value = Math.round(evil(print.value) * 10000) / 10000;
			erase = true;
		}
	}
	else {
		if (erase) {
			print.value = "";
			erase = false;
		}
		print.value += x;
	}
}

function evil(fn) {
	return new Function('return ' + fn)();
}
// -------------------------- END of Calculator ----------------------- //


function initiateJSONThemes() {
	var themesLists = '[' + 
    '{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},' +
    '{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},' +
    '{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},' +
    '{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},' +
    '{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},' +
    '{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},' +
    '{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},' +
    '{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},' +
    '{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},' +
    '{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},' +
    '{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}' +
	']';
	var selectedTheme = '{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}';
	window.localStorage.setItem('themesLists',themesLists);
	window.localStorage.setItem('selectedTheme', selectedTheme);
}


function applyClickHandler(id) {
	var themes = JSON.parse(window.localStorage.getItem('themesLists'));
	
	// mengambil thema yang dipilih
	var choosedTheme = themes.filter(function(item) {
		return item.id == id;
	})[0];
	
	window.localStorage.setItem('selectedTheme', JSON.stringify(choosedTheme));
	applyTheme();
}

function applyTheme() {
	var choosedTheme = JSON.parse(window.localStorage.getItem('selectedTheme'));

	// apply change
	$('body').css('background-color', choosedTheme.bcgColor);
	$('body').css('font-color', choosedTheme.fontColor);
}

$(document).ready(function() {
	print = document.getElementById('print');
	erase = false;
	// --------------------------- ChatBox ------------------------------ //
	var cls = "msg-receive";
    //nambahin chat text setelah di tekan 'enter'
    $('.chat-text-area').keypress(function (e) {
        if ((e.keyCode || e.which) == 13) {
            var msg = $('.chat-text-area').val();
            var old = $('.msg-insert').html();
            if (msg.length == 0) {
                alert("Message kosong");
            }
            else {
                cls = (cls == "msg-send") ? "msg-receive" : "msg-send";
                $('.msg-insert').html(old + '<p class=' + cls + '>' + msg + '</p>')
                $('.chat-text-area').val("");

            }
        }
    });

    //reset ulang text-area chat setelah enter
    $('.chat-text-area').keyup(function (e) {
        if ((e.keyCode || e.which) == 13) {
            $('.chat-text-area').val("");
        }
    });

    // -------------------------- END of ChatBox -------------------------- //
	

	initiateJSONThemes();

	$('.my-select').select2({
		'data': JSON.parse(window.localStorage.getItem('themesLists'))
	});

	$('.apply-button').click(function(){
		applyClickHandler($('.my-select').val());
	});
	applyTheme();
});