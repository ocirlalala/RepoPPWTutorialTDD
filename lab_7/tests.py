from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Friend
from .views import (
	index, friend_list, add_friend, delete_friend, validate_npm, paginate_page
)
from .api_csui_helper.csui_helper import CSUIhelper
from unittest.mock import patch
from django.db.models.manager import Manager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os

class Lab7Test(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code,200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab_7_add_friend(self):
		response = Client().post(
			'/lab-7/add-friend/',
			{'name':'rico', 'npm':'16068'}
		)
		self.assertEqual(response.status_code, 200)

	def test_lab_7_validate_npm(self):
		response = Client().post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})

	def test_lab_7_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_get_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_delete_friend(self):
		friend = Friend.objects.create(friend_name='Ocir', npm='16068')
		response = Client().post('/lab-7/delete-friend/' + str(16068) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	def test_lab_7_Friends_as_dict(self):
		friend = Friend(friend_name="Rico", npm="1234567890")
		friend_list = Friend.objects.all()
		self.assertEqual(friend.as_dict(), {"friend_name":"Rico","npm":"1234567890"})

	def test_invalid_page_pagination_number(self):
		data = ["a","b","c","d","e"]
		test1 = paginate_page("...", data)
		test2 = paginate_page(-1, data)
		with patch.object(Manager, 'get_or_create') as a:
			a.side_effect = PageNotAnInteger("page number is not an integer")
			res = paginate_page("...", data)
			self.assertEqual(res['page_range'], test1['page_range'])
			with patch.object(Manager, 'get_or_create') as a:
				a.side_effect = EmptyPage("page number is less than 1")
				res = paginate_page(-1, data)
				self.assertEqual(res['page_range'], test2['page_range'])

	def test_lab_7_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])