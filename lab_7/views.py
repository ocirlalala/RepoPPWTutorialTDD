from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()

    # this will assume, if no page selected, it is on the first page
    page = request.GET.get('page', 1)
    paginate_data = paginate_page(page, mahasiswa_list)
    mahasiswa = paginate_data['data']
    page_range = paginate_data['page_range']

    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list, "page_range": page_range}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def paginate_page(page, lst):
    paginator = Paginator(lst, 10)

    # assign data value with page
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # index of curr page
    index_now = data.number - 1

    # max index of your pages, last page - 1
    max_index = len(paginator.page_range) #page_range returns an iterator

    # get start and last index for each slices/pages of the lst
    start_index = index_now if index_now >= 10 else 0
    end_index = 10 if index_now > max_index - 10 else max_index

    # get out new page_range thus pass it to list to make our slice possible again
    page_range = list(paginator.page_range)[start_index:end_index]

    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data

def friend_list(request):
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request):
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        isAdaFriend = Friend.objects.filter(npm=npm).exists()
        if not isAdaFriend:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
        data = model_to_dict(friend)
        print(friend)
        return HttpResponse(data)

def delete_friend(request, npm):
    Friend.objects.filter(npm=npm).delete()
    return HttpResponseRedirect('/lab-7/friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': (Friend.objects.filter(npm=npm).exists()), #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

# 
def model_to_dict(obj):
    data = serializers.serialize('json', [obj,]) #
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

