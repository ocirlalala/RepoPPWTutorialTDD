// FB initiation function
var picture, name;
window.fbAsyncInit = () => {
  FB.init({
    appId      : '300081860503004',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      console.log(response.status);
      /* the user is logged in and has authenticated your
       app, and response.authResponse supplies
       the user's ID, a valid access token, a signed
       request, and the time the access token 
       and signed request each expire */
      render(true);
    } else if (response.status === 'not_authorized') {
      // the user is logged in to Facebook, 
      // but has not authenticated your app
      console.log('not author');
      render(false);
    } else {
      // the user isn't logged in to Facebook.
      console.log('not login');
      render(false);
    }
  });

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      console.log(user);
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<section id="section-profile" class="profile">' +
          '<div class="row cover-photo">' +
            '<div id="cover-photo" class="text-center">' +
              '<img id="cov-photo" src="' + user.cover.source + '" alt="cover" />' +
            '</div>' +
          '</div>' +
          '<div class="row " id="pic-desc">' +
            '<div class="col-xl-4 col-md-4 text-center picture-photo">' +
              '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
              '<h1>' + user.name + '</h1>' +               
            '</div>' +
            '<div class="col-xl-8 col-md-8 description">' +
              '<div class="about">' +
                '<img src="https://png.icons8.com/user/win10/25/000000"> <b>About Me</b>' +
                '<p>' + user.about + '</p>' +
              '</div>' +
              '<div class="email">' +
                '<img src="https://png.icons8.com/message-filled/win10/25/000000"> <b>E-mail</b>' +
                '<p>' + user.email +
              '</div>' +
              '<div class="gender">' +
                '<img src="https://png.icons8.com/gender/win10/25/000000"> <b>Gender</b>' + 
                '<p>' + user.gender + '</p>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="row post-div">' +
            '<h1 id="title-post">Post to Facebook</h1>' +
            '<input id="postInput" type="text" class="post form-control" placeholder="Ketik Status Anda" />' +
            '<button class="postStatus btn btn-success" onclick="postStatus()">Post</button>' +
            '<button class="logout btn btn-primary" onclick="facebookLogout()">Logout</button>' +
          '</div>' +
        '</section>' +
        '<section id="section-feed" class="feed">' +
        '</section>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        
        feed.data.map(value => {
          console.log('value:' + value.id);
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('.feed').append(
              '<div class="user-feed">' +
                '<div class="user-pic">' +
                  '<img id="user-pic" src="' + picture + '">' + '<b> ' + name + '</b>' +
                '</div>' +
                '<div class="feed-desc">' +
                  '<h1 class="desc-font">' + value.message + '</h1>' +
                  '<h2 class="desc-font">' + value.story + '</h2>' +
                '</div>' +
              '</div>'
            );
          } else if (value.message) {
            $('.feed').append(
              '<div class="user-feed">' +
                '<div class="user-pic">' +
                   '<img id="user-pic" src="' + picture + '">' + '<b> ' + name + '</b>' +
                '</div>' +
                '<div class="feed-desc">' +
                  '<h1 class="desc-font">' + value.message + '</h1>' +
                '</div>' +
              '</div>'
            );
          } else if (value.story) {
            $('.feed').append(
              '<div class="user-feed">' +
                '<div class="user-pic">' +
                   '<img id="user-pic" src="' + picture + '">' + '<b> ' + name + '</b>' +
                   
                '</div>' +
                '<div class="feed-desc">' +
                  '<h2 class="desc-font">' + value.story + '</h2>' +
                '</div>' +
              '</div>'
            );
          } 
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html(
      '<div class="container-fluid text-center before-login">' +
        '<h1 style="font-family:anton"> Welcome to MyPesbuk!</h1>' +
        '<button class="login btn btn-primary" onclick="facebookLogin()">Login</button>' +
      '</div>'
    );
      
  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  FB.login(function(response){
     console.log(response);
     render(true);
   }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
  // user_posts : Menyediakan akses ke kiriman di Linimasa seseorang.
  // publish_actions : Menyediakan akses untuk menerbitkan Kiriman,
};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
            FB.logout();
            render(false);
            swal({
              text: "You have been successfully logged out!",
              icon: "success"
            });
    }
  }); 
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(160).height(160)', 'GET', function (response){
      console.log(response);
      if (response && !response.error) {
        picture = response.picture.data.url;
        name = response.name;
        about = response.about;
        userId = response.id;
        console.log("about: " + about);
        fun(response);
      }
      else {
        // sweet alert
        swal({
          text: "Something went wrong!",
          icon: "error"
        });
      }
  });
    }
  })
};

const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.getLoginStatus(function(response) { // sumber : https://developers.facebook.com/docs/graph-api/reference/v2.11/user/feed
    if (response.status === 'connected') {
      // FB.api('/me/feed', 'GET', function(response))
      FB.api('/me/feed', 'GET', function(response) { 
        if (response && !response.error) {
          /* handle the result */
          console.log('response feed: ' + response);
          fun(response);
        }
      })
    }
    else {
      swal({
        text: "Something went wrong!",
        icon: "error"
      });
    }
  });
};

const postFeed = (message) => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  FB.api('/me/feed', 'POST', {'message':message}, function(response) { // sumber : https://developers.facebook.com/docs/graph-api/reference/v2.11/user/feed
    if (response && !response.error) {
      swal({
        text: "Your post has been posted",
        icon: "success"
      });
    }
  });
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};
